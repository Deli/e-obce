# UI42 Zadanie

## Requirements
- Composer
- NPM

This application is built using Laravel 10 framework, which requires PHP version 8.1 or higher.


## Installation
### 1. Clone repository

```shell
git clone git@gitlab.com:Deli/e-obce.git
```
### 2. Navigate to Project Directory
```shell
cd e-obce
```


### 3. Install Composer/NPM Dependencies
```shell
composer install
npm install
```


### 5. Configure Environment Variables
Copy the .env.example file and fill out the necessary environment variables, such as database settings and Google API key:
```shell
cp .env.example .env

# edit using your editor of choice 
nano .env
```

```env
# .env

DB_DATABASE=database
DB_USERNAME=user
DB_PASSWORD=password

GOOGLE_API_KEY=apikey
```

### 6. Generate Application Key
```shell
php artisan key:generate
```



### 7. Build Vite Assets
```shell
npm run build
```
### 8. Link storage
```shell
php artisan storage:link
```

### 9. Running migrations
```shell
php artisan migrate
```

### 10. Serve the Application locally (Optional)
```shell
php artisan serve
```


## Importing data

### 0. Driver selection (Optional)
By default import and geocode jobs run on sync queue but jobs migration is included.
To run jobs on database driver change `QUEUE_CONNECTION` in `.env` file.

```shell
QUEUE_CONNECTION=database
```

Then run queue worker command:
```shell
php artisan queue:listen
# or in prod use
php artisan queue:work
```

### 1. Import cities
To import cities from any district use `data:import` command.
You can specify which district do load using short name.


Available options:
- `BB` or `bb` -> Banskobystrický kraj
- `BA` or `ba` -> Bratislavský kraj
- `KE` or `ke` -> Košický kraj,
- `NR` or `nr` -> Nitriansky kraj,
- `PO` or `po` -> Prešovský kraj,
- `TN` or `tn` -> Trenčiansky kraj, 
- `TT` or `tt` -> Trnavský kraj,
- `ZA` or `za` -> Žilinský kraj
```shell

# default: Nitriansky kraj
php artisan data:import 

# Košický kraj
php artisan data:import ke

# Prešovský kraj
php artisan data:import po

# etc..
```


### 2. Geocode cities
If `GOOGLE_API_KEY` is specified in .env you can use `data:geocode` command to get lat/lng coordinates for each city
```shell
php artisan data:geocode
```

### 3. Scheduling
Sine `data:geocode` command only processes cities that do not have lat/lng coordinates we can setup scheduler to run this command periodically to fill up missing data after importing more cities.
Example setup in `app/Console/Kernel.php`.
```php
class Kernel extends ConsoleKernel
{
    protected function schedule(Schedule $schedule): void
    {
        // $schedule->command('data:geocode')->hourly();
    }
```
Remember to run the scheduler.
```shell
# Run scheduler locally
php artisan schedule:work
```
Or refer to [Laravel Documentation](https://laravel.com/docs/10.x/scheduling#running-the-scheduler) to run the scheduler using cron.

<!-- TODO: header styling, readme about Queue setup,  -->



## Screenshots

### Desktop
<details>
<summary>Index</summary>
<img src='screenshots/index.png'>
</details>

<details>
<summary>Search</summary>
<img src='screenshots/main_search.png'>
</details>

<details>
<summary>Navbar Search</summary>
<img src='screenshots/header_search.png'>
</details>

<details>
<summary>City list</summary>
<img src='screenshots/city_list.png'>
</details>

<details>
<summary>City list by district</summary>
<img src='screenshots/city_list_by_district.png'>
</details>

<details>
<summary>City list by subdistrict</summary>
<img src='screenshots/city_list_by_subdistrict.png'>
</details>

<details>
<summary>City details</summary>
<img src='screenshots/cidy_details.png'>
</details>

### Mobile

<details>
<summary>Index</summary>
<img src='screenshots/mobile_index.png'>
</details>

<details>
<summary>Navigation</summary>
<img src='screenshots/mobile_navigation.png'>
</details>

<details>
<summary>City list</summary>
<img src='screenshots/mobile_city_list.png'>
</details>

<details>
<summary>City details</summary>
<img src='screenshots/mobile_city_details.png'>
</details>
