<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CityController;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\SubdistrictController;
use App\Http\Controllers\LocaleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('index');

// Districts
Route::resource('district', DistrictController::class)->only(['show']);

// Subdistricts
Route::resource('subdistrict', SubdistrictController::class)->only(['show']);

// Cities
Route::prefix('city')->group(function () {
    Route::controller(CityController::class)->group(function () {
        Route::get('/autocomplete', 'autocomplete')->name('city.autocomplete');
    });
});
Route::resource('city', CityController::class)->only(['index', 'show']);


// Locale
// Reading the locale from the session is handled by middleware SetLocale
Route::prefix('locale')->group(function () {
    Route::controller(LocaleController::class)->group(function () {
        Route::get('/{locale}', 'set')->name('locale.set');
    });
});

// Catch-all
Route::fallback(function () {
    return redirect(route('index'));
});
