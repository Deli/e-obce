<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>UI42</title>

    {{-- CSS/JS --}}
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"></script>
    @vite(['resources/scss/app.scss', 'resources/js/app.js'])

    {{-- FA ICONS --}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css"
        integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />


    @stack('head')

</head>

<body>

    <x-partials.header />

    <main>
        {{ $slot }}
    </main>

    <x-partials.footer />

    @stack('scripts')

</body>

</html>
