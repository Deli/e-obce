<nav class="navbar navbar-expand-lg mt-3 border-bottom py-2">
    <div class="container">
        <div class="col">

            {{-- TOP LINE --}}

            <div class="navbar justify-content-between ">
                {{-- LEFT --}}
                <a href="/" title="Logo" class="navbar-brand">
                    <x-partials.header-logo />
                </a>

                {{-- RIGHT --}}
                <div class="d-flex align-items-baseline ">
                    <div class="navbar-nav navbar-collapse collapse ">

                        <a href=" #" class="nav-item btn text-primary fw-bold text-nowrap ">
                            {{ __('Contacts and department numbers') }}
                        </a>

                        <x-header.lang-select />

                        <x-header.search />

                        {{-- Login --}}
                        <a class="btn btn-login px-4 ms-3 nav-item" href="#">{{ __('Login') }}</a>

                    </div>

                    <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvas"
                        aria-controls="offcanvas">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                </div>

            </div>


            {{-- SECOND LINE --}}
            <div class="mt-4">
                {{-- Links --}}
                {{-- ADD TO OFFCANVAS --}}
                <div class="offcanvas offcanvas-end px-0" tabindex="-1" id="offcanvas">
                    <div class="offcanvas-header bg-primary text-white">
                        <h5 class="offcanvas-title mx-auto " id="offcanvasDarkNavbarLabel">
                            {{ __('Menu') }}
                        </h5>
                        <button type="button" class="btn-close btn-close-white" data-bs-dismiss="offcanvas"
                            aria-label="Close"></button>
                    </div>
                    <ul class=" navbar-nav fw-medium text-center text-lg-start ">
                        <li class="py-3 py-lg-0 nav-item">
                            <a href="#" class="nav-link ps-0 pe-4 py-0">{{ __('About us') }}</a>
                        </li>
                        <li class="py-3 py-lg-0 nav-item">
                            <a href="{{ route('city.index') }}" class="nav-link ps-0 pe-4 py-0">{{ __('List of cities')
                                }}</a>
                        </li>
                        <li class="py-3 py-lg-0 nav-item">
                            <a href="#" class="nav-link ps-0 pe-4 py-0">{{ __('Inspection') }}</a>
                        </li>
                        <li class="py-3 py-lg-0 nav-item">
                            <a href="#" class="nav-link ps-0 pe-4 py-0">{{ __('Contact') }}</a>
                        </li>
                    </ul>
                    <div class="offcanvas-body d-lg-none">
                        <a href=" #" class="d-block mx-auto my-3 nav-item btn text-primary fw-bold text-nowrap ">
                            {{ __('Contacts and department numbers') }}
                        </a>

                        <x-header.search />

                        {{-- Login --}}
                        <a class="d-block btn btn-login px-4 my-3 nav-item" href="#">{{ __('Login') }}</a>


                        <x-header.lang-select />

                    </div>
                </div>

            </div>


        </div>


    </div>
</nav>