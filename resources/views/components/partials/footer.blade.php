<footer class="bg-light-gray text-secondary">
    <div class="container py-5">
        <div class="row">

            <ul class="col-12 col-md-6 col-lg-4 list-unstyled">
                <li class="fw-bold">{{ __('ADDRESS AND CONTACT') }}</li>
                <li>ŠÚKL</li>
                <li>Kvetná 11</li>
                <li>821 08 Bratislava 26</li>
                <li>Ústredňa: 00421/2/50 70 11 11</li>

                <li class="fw-bold mt-3">KONTAKTY</li>
                <li>Telefónne čísla</li>
                <li>Regionálne pracoviská</li>
                <li>Úradné hodiny</li>
                <li>Bankové spojenie</li>

                <li class="fw-bold mt-3">INFORMÁCIE PRE VEREJNOSŤ</li>
                <li>Zoznam oznámení o prvom uvedení, prerušení, zrušení a obnovení dodávok liekov</li>
                <li>Zoznam vyvezených liekov</li>
                <li>MZ SR</li>
                <li>Národný portál zdravia</li>
                <li>Zoznam prijatých žiadostí o novú registráciu</li>
                <li>Zoznam zrušených registrácií liekov</li>
                <li>Odber noviniek</li>
            </ul>
            <ul class="col-12 col-md-6 col-lg-4 list-unstyled">
                <li class="fw-bold">O NÁS</li>
                <li>Dotazníky</li>
                <li>Hlavní predstavitelia</li>
                <li>Základné dokumenty</li>
                <li>Zmluvy za ŠÚKL</li>
                <li>História a súčasnosť</li>
                <li>Národná spolupráca</li>
                <li>Spolupráca s pacientskymi organizáciami</li>
                <li>Medzinárodná spolupráca</li>
                <li>Poradné orgány</li>
                <li>Legislatíva</li>
                <li>Sadzobník ŠÚKL</li>
                <li>Verejné obstarávanie</li>
                <li>Vzdelávacie akcie a prezentácie</li>
                <li>Konzultácie</li>
                <li>Voľné pracovné miesta</li>
                <li>Poskytovanie informácií</li>
                <li>Sťažnosti a petície</li>
            </ul>
            <ul class="col-12 col-md-6 col-lg-4 list-unstyled">
                <li class="fw-bold">MÉDIÁ</li>
                <li>Tlačové správy</li>
                <li>Kontakt pre médiá</li>

                <li class="fw-bold mt-3">DATABÁZY A SERVIS</li>
                <li>Vyhľadávanie liekov, zdravotníckych pomôcok a zmien v liekovej databáze</li>
                <li>Iné zoznamy</li>
                <li>Kontaktný formulár</li>
                <li>Mapa stránok</li>
                <li>A - Z index</li>
                <li>Linky</li>
                <li>RSS</li>
                <li>Doplnok pre internetový prehliadač</li>
                <li>Prehliadače formátov</li>
            </ul>
            <ul class="col-12 col-md-6 col-lg-4 list-unstyled">
                <li class="fw-bold">DROGOVÉ PREKURZORY</li>
                <li>Aktuality</li>
                <li>Legislatíva</li>
                <li>Pokyny</li>
                <li>Kontakt</li>

                <li class="fw-bold mt-3">INÉ</li>

                <li>Linky</li>
                <li>Mapa stránok</li>
                <li>FAQ</li>
                <li>Podmienky používania stránok</li>
                <li>Správca obsahu: spravca@sukl.sk</li>
                <li>Technický prevádzkovateľ ui42 spol.s.r.o.</li>

                <li class="fw-bold text-primary mt-3">RAPID ALERT SYSTEM</li>
                <li class="text-primary">Rýchla výstraha vyplývajúca z nedostatkov v kvalite liekov</li>
            </ul>

        </div>
    </div>
</footer>