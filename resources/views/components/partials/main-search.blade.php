<h1 class="display-3 mb-4 fw-light">{{ __('Search in city database') }}</h1>
<div class="searchbar col-12 col-md-6 col-lg-4 mx-auto ">
    <form autocomplete="off" action="{{ route('city.index') }}" method="get">
        <input type="text" class="shadow form-control form-control-lg city-search" name="search"
            placeholder="{{ __('Enter name') }}">
        <ul class="list-group city-list"></ul>
    </form>
</div>