<section>
    <div class="container-fluid text-center hero-gradient text-white d-flex flex-column justify-content-center">
        <div class="py-5">
            <div class="mx-auto">

                {{ $slot }}

            </div>
        </div>
    </div>
</section>