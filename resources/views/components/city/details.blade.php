<div class="row shadow mb-5">
    <div class="col-12 col-md-6 order-md-1 order-2 bg-light p-md-5  ">

        <table class="table table-sm table-borderless table-light">
            <tbody>
                <tr>
                    <td class="d-block d-lg-table-cell fw-bold">
                        {{ __('Mayor name') }}:
                    </td>
                    <td class="d-block d-lg-table-cell">
                        {{ $city->mayor ?? __('Unknown') }}
                    </td>
                </tr>


                <tr>
                    <td class="d-block d-lg-table-cell fw-bold">
                        {{ __('Hall address') }}:
                    </td>
                    <td class="d-block d-lg-table-cell">
                        {{ $city->hall_address ?? __('Unknown') }}
                    </td>
                </tr>

                @if ($city->phone)
                <tr>
                    <td class="d-block d-lg-table-cell fw-bold">
                        {{ __('Phone') }}:
                    </td>
                    <td class="d-block d-lg-table-cell">
                        {{ $city->phone ?? __('Unknown') }}
                    </td>
                </tr>
                @endif

                @if ($city->fax)
                <tr>
                    <td class="d-block d-lg-table-cell fw-bold">
                        {{ __('Fax') }}:
                    </td>
                    <td class="d-block d-lg-table-cell">
                        {{ $city->fax ?? __('Unknown') }}
                    </td>
                </tr>
                @endif

                @if ($city->email)
                <tr>
                    <td class="d-block d-lg-table-cell fw-bold">
                        {{ __('E-mail') }}:
                    </td>
                    <td class="d-block d-lg-table-cell">
                        {{ $city->email ?? __('Unknown') }}
                    </td>
                </tr>
                @endif


                @if ($city->web)
                <tr>
                    <td class="d-block d-lg-table-cell fw-bold">
                        {{ __('Web address') }}:
                    </td>

                    <td class="d-block d-lg-table-cell">
                        <a href="{{ $city->web ?? __('#') }}" target="_blank" rel="noopener noreferrer">
                            {{ $city->web ?? __('Unknown') }}
                        </a>
                    </td>
                </tr>
                @endif

                @if ($city->district())
                <tr>
                    <td class="d-block d-lg-table-cell fw-bold">
                        {{ __('District') }}:
                    </td>

                    <td class="d-block d-lg-table-cell">
                        <a href="{{ route('district.show',$city->district) }}">
                            {{ $city->district->name }}
                        </a>
                    </td>
                </tr>
                @endif

                @if ($city->subdistrict)
                <tr>
                    <td class="d-block d-lg-table-cell fw-bold">
                        {{ __('Subdistrict') }}:
                    </td>

                    <td class="d-block d-lg-table-cell">
                        <a href="{{ route('subdistrict.show',$city->subdistrict) }}">
                            {{ $city->subdistrict->name }}
                        </a>
                    </td>
                </tr>
                @endif

                @if ($city->lat && $city->lng)
                <tr>
                    <td class="d-block d-lg-table-cell fw-bold">
                        {{ __('Geographic coordinates') }}
                    </td>
                    <td class="d-block d-lg-table-cell">
                        <a href="https://www.google.com/maps/search/?api=1&query={{ $city->lat }}%2C{{ $city->lng }}"
                            target="_blank" rel="noopener noreferrer">
                            {{ $city->lat ?? '-' }},
                            {{ $city->lng ?? '-' }}
                        </a>
                    </td>
                </tr>
                @endif
            </tbody>
        </table>

    </div>
    <div class="col-12 col-md-6 order-md-2 order-1 p-5 d-flex flex-column justify-content-center">
        <img src="{{ asset('storage/' . $city->image) }}" width="100px" class="mx-auto" alt="">
        <div class="text-center fw-bold h1 mt-2 text-primary">
            {{ $city->name ?? __('Unknown') }}
        </div>
    </div>
</div>