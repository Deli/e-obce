    <div class="col-6 col-md-4 col-lg-3">
        <a class="text-decoration-none" href="{{ route('city.show', $city->slug) }}">
            <div class=" shadow my-3 p-4 city-simple  text-secondary">
                <img src="{{ asset('storage/' . $city->image) }}" class="d-block mx-auto" alt="">
                <div class="city-simple-content text-center fw-light h2 mt-3">
                    {{ $city->name ?? __('Unknown') }}
                </div>

            </div>
        </a>
    </div>
