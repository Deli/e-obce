<div class="header-search">
    <form autocomplete="off" action="{{ route('city.index') }}" method="get" class="searchbar">
        <input type="text" name="search" class="city-search form-control">
        <i class="fa-solid fa-magnifying-glass"></i>
        <ul class="list-group city-list"></ul>

    </form>

</div>
