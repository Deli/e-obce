<select id="langSelect" class="nav-item lang-select text-secondary me-2">
    <option value="en" @selected(session()->get('locale', config('app.locale')) == 'en')>EN</option>
    <option value="sk" @selected(session()->get('locale', config('app.locale')) == 'sk')>SK</option>
</select>