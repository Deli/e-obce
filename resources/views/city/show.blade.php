<x-layout.main>
    <div class="container">
        <div class="row my-4 text-center text-secondary">
            <h1 class="fw-light">{{ __('City details') }}</h1>
        </div>

        <x-city.details :$city />
    </div>


</x-layout.main>
