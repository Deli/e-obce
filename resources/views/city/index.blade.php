<x-layout.main>
    <div class="container">
        <div class="row my-4 text-center text-secondary">
            <h1 class="fw-light ">
                {{ __('City list') }}
            </h1>

            {{-- SUBTITLE --}}

            @if(request()->get('search'))
            <h2 class="fw-light ">
                {{ __('Searching') }}
                "{{ request()->get('search') }}"
            </h2>
            @endif
            @if(isset($district) || isset($subdistrict))
            <h2 class="fw-light ">
                {{ $district->name ?? ''}}

                {{-- Separator --}}
                @if (isset($district) && isset($subdistrict))
                |
                @endif

                {{ $subdistrict->name ?? ''}}
            </h2>
            @endif

            {{-- Subdistrict links --}}
            @if(isset($district) && empty($subdistrict))
            <div class="my-3">
                @foreach ($district->subdistricts as $subdistrict)
                <a class="px-2 btn btn-sm btn-primary" href="{{ route('subdistrict.show',$subdistrict) }}">
                    {{ $subdistrict->name }}
                </a>
                @endforeach
            </div>
            @endisset


        </div>
        <div class="row">

            @forelse ($cities as $city)
            <x-city.simple :$city />
            @empty
            <div class="text-center my-5">
                {{ __('No city found') }}
            </div>
            @endforelse
        </div>

        {{ $cities->links() }}
    </div>


</x-layout.main>