// langSelect.js

// Language changer event

$(function () {
    $("#langSelect").on("change", function () {
        let locale = $(this).val();
        $.get("/locale/" + locale).then(function () {
            window.location.reload();
        });
    });
});
