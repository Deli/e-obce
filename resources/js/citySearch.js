// citySearch.js

// Basic dropdown search suggestions for searching cities
// 
// Needs refactoring
// 
// Switch to Select2 if needed

$(function () {
    $(".city-search").each(function () {
        $(this).on("focus", function () {
            $(this).siblings(".city-list").show();
        });

        $(this).on('keydown', function (e) {
            let list = $(this).siblings(".city-list");
            switch (e.keyCode) {
                case 40: // down
                    // Check if something is already selected
                    var selection = list.children('.list-group-item.selected');
                    if (!selection.length) {
                        // select first
                        list.children('li:first').eq(0).addClass('selected');
                    } else {
                        //select next
                        selection.next().addClass('selected');
                        selection.removeClass('selected');
                    }
                    break;
                case 38: // up
                    var selection = list.children('.list-group-item.selected');
                    selection = list.children('.list-group-item.selected');
                    selection.prev().addClass('selected');
                    selection.removeClass('selected');
                    break;


                case 13: // Enter
                    var selection = list.children('.list-group-item.selected');
                    if (selection.length) {
                        e.preventDefault();
                        window.location.href = selection.attr('data-route')
                    }

            }
        });

        $(this).on("input", function () {
            let query = $(this).val();
            let list = $(this).siblings(".city-list");

            // Ignore input while processing
            if (list.hasClass("loading")) {
                return;
            }
            list.addClass("loading");

            // Clear on empty
            list.empty();

            // If no query then stop
            if (!query) {
                list.removeClass("loading");
                return;
            }

            $.get(
                "/city/autocomplete",
                {
                    query: query,
                },
                function (data) {
                    data.forEach((city) => {
                        let li = $("<li></li>", {
                            class: "list-group-item",
                            value: city.slug,
                            "data-route": city.route,
                        })
                            .html(city.name)
                            .on("click", function () {
                                let url = $(this).attr("data-route");
                                console.log(url);
                                window.location.href = url;
                            })
                            .on('mouseenter', function () {
                                $(this).siblings('.selected').removeClass('selected');
                                $(this).addClass('selected');
                            });
                        list.append(li);
                    });
                    list.removeClass("loading");
                }
            );
        });
    });

    // Hide suggestions on focus loss
    $(document).on("click", function (event) {
        if (
            !$(event.target).closest(".city-search").length &&
            !$(event.target).closest(".city-list").length
        ) {
            $(".city-search").siblings(".city-list").hide();
            // cancel selection
            $(".city-search").siblings(".city-list").find('.selected').each(function () {
                $(this).removeClass('.selected')
            });
        }
    });
});
