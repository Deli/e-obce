<?php

namespace Tests\Feature;

use App\Models\City;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CityAutocompleteTest extends TestCase
{
    use RefreshDatabase;

    private $searchedCity;
    private $similarCity;
    private $otherCity;

    public function setUp(): void
    {
        parent::setUp();
        $this->searchedCity = City::factory()->create(['name' => 'ABCDE', 'slug' => 'abcde']);
        $this->similarCity = City::factory()->create(['name' => 'ABCDE QW', 'slug' => 'abcde_qw']);
        $this->otherCity = City::factory()->create(['name' => 'ZXCV', 'slug' => 'zxcv']);

        // Populate with random cities
        // City::factory()->count(3)->create();
    }

    public function test_autocompolete_search_city(): void
    {
        $response = $this->get(route('city.autocomplete', ['query' => 'abc']));

        $response->assertStatus(200)
            ->assertJsonCount(2)
            ->assertJson([
                ['name' => $this->searchedCity->name],
                ['name' => $this->similarCity->name],
            ]);
    }

    public function test_autocompolete_search_empty_sting(): void
    {
        $response = $this->get(route('city.autocomplete', ['query' => '']));

        $response->assertStatus(200)
            ->assertJsonCount(City::limit(5)->count()); // check if all are returned (limit 5)
    }

    public function test_autocompolete_search_invalid_city(): void
    {
        $response = $this->get(route('city.autocomplete', ['query' => 'abcqwe']));

        $response->assertStatus(200)
            ->assertJsonCount(0);
    }
}
