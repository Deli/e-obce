<?php

namespace Tests\Feature;

use App\Models\City;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CityPageTest extends TestCase
{
    use RefreshDatabase;

    private $cities;

    public function setUp(): void
    {
        parent::setUp();
        $this->cities = City::factory()->count(3)->create();
    }

    public function test_index_page_has_all_cities()
    {
        $response = $this->get(route('city.index'));

        $response->assertStatus(200);

        foreach ($this->cities as $city) {
            $response->assertSee($city->name);
        }
    }

    public function test_index_page_shows_no_results()
    {
        City::truncate(); // Ensures that there are no cities in the database

        $response = $this->get(route('city.index'));

        $response->assertStatus(200)
            ->assertSee(__('No city found'));
    }

    public function test_show_page_has_correct_data()
    {
        $city = $this->cities[0];
        $response = $this->get(route('city.show', $city));

        $response->assertStatus(200)
            ->assertDontSee($city->url)
            ->assertSee($city->name)
            ->assertSee($city->mayor)
            ->assertSee($city->phone)
            ->assertSee($city->fax)
            ->assertSee($city->email)
            ->assertSee($city->web)
            ->assertSee($city->hall_address);
    }

    public function test_invalid_city_returns_404()
    {
        $response = $this->get(route('city.show', "invalid"));

        $response->assertStatus(404);
    }
}
