<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class IndexTest extends TestCase
{
    public function test_status(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_hero_text(): void
    {
        $responseEn = $this->followingRedirects()->from('/')->get('/locale/en');
        $responseSk = $this->followingRedirects()->from('/')->get('/locale/sk');

        $responseEn->assertStatus(200);
        $responseEn->assertSee('Search in city database');
        $responseSk->assertStatus(200);
        $responseSk->assertSee('Vyhľadať v databáze obcí');
    }
}
