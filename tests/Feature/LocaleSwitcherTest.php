<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LocaleSwitcherTest extends TestCase
{
    public function test_switch_to_en(): void
    {
        $response = $this->get('/locale/en');
        $response->assertSessionHas('locale', 'en');
    }

    public function test_switch_to_sk(): void
    {
        $response = $this->get('/locale/sk');
        $response->assertSessionHas('locale', 'sk');
    }

    public function test_switch_to_unknown_fallbacks_to_sk(): void
    {
        $response = $this->get('/locale/unknown');
        $response->assertSessionHas('locale', 'sk');
    }
}
