<?php

namespace Tests\Unit;

use App\Services\Eobce\Subdistrict;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EobceSubdistrictTest extends TestCase
{
    use RefreshDatabase;

    private Subdistrict $subdistrict;
    private string $filePath = __DIR__ . '/eobce/subdistrict.html';

    public function setUp(): void
    {
        parent::setUp();
        $this->subdistrict = new Subdistrict($this->filePath);
    }



    public function test_initialization(): void
    {
        $data = $this->subdistrict->getData();

        $this->assertEquals([
            'url' => $this->filePath,
            'loaded' => 'NO',
        ], $data);
    }

    public function test_loading(): void
    {
        $this->subdistrict->load();

        $this->assertEquals($this->subdistrict->name, 'Poprad');
        $this->assertTrue($this->subdistrict->isParsed());
        $this->assertEquals(count($this->subdistrict->cities), 21);
    }

    public function test_get_data(): void
    {
        $this->subdistrict->load();

        $data = $this->subdistrict->getData();

        $this->assertEquals($data['loaded'], "YES");
        $this->assertEquals($data['name'], 'Poprad');
        $this->assertEquals(count($data['cities']), 21);
    }
}
