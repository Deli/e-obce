<?php

namespace Tests\Unit;

use App\Services\Eobce\City;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EobceCityTest extends TestCase
{
    use RefreshDatabase;

    private City $city;
    private string $filePath = __DIR__ . '/eobce/city.html';


    public function setUp(): void
    {
        parent::setUp();
        $this->city = new City($this->filePath);
    }



    public function test_initialization(): void
    {
        $data = $this->city->getData();

        $this->assertEquals([
            'url' => $this->filePath,
            'loaded' => 'NO',
        ], $data);
    }

    public function test_loading(): void
    {
        $this->city->load();

        $this->assertEquals($this->city->name, 'Poprad');
        $this->assertTrue($this->city->isParsed());
    }

    public function test_get_data(): void
    {
        $this->city->load();

        $data = $this->city->getData();

        $this->assertEquals($data, [
            'url' => $this->filePath,
            'loaded' => 'YES',
            'name' => 'Poprad',
            'hall_address' => 'Nábrežie Jána Pavla II. 2082/3',
            'web' => 'https://www.poprad.sk',
            'email' => 'podatelna@msupoprad.sk',
            'mayor' => 'Ing. Anton Danko',
            'phone' => '052 / 716 71 11',
            'fax' => '052 / 772 12 18, 716 72 04',
            'logoUrl' => 'https://www.e-obce.sk/erb/1889.gif',
        ]);
    }
}
