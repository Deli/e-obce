<?php

namespace Tests\Unit;

use App\Services\Eobce\District;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class EobceDistrictTest extends TestCase
{
    use RefreshDatabase;

    private District $district;
    private string $filePath = __DIR__ . '/eobce/district.html';

    public function setUp(): void
    {
        parent::setUp();
        $this->district = new District($this->filePath);
    }



    public function test_initialization(): void
    {
        $data = $this->district->getData();

        $this->assertEquals([
            'url' => $this->filePath,
            'loaded' => 'NO',
        ], $data);
    }

    public function test_loading(): void
    {
        $this->district->load();

        $this->assertEquals($this->district->name, 'Prešovský kraj');
        $this->assertTrue($this->district->isParsed());
        $this->assertEquals(count($this->district->subdistricts), 13);
    }

    public function test_get_data(): void
    {
        $this->district->load();

        $data = $this->district->getData();

        $this->assertEquals($data['loaded'], "YES");
        $this->assertEquals($data['name'], 'Prešovský kraj');
        $this->assertEquals(count($data['subdistricts']), 13);
    }
}
