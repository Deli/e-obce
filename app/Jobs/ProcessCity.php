<?php

namespace App\Jobs;

use App\Models\City;
use App\Models\District;
use App\Models\Subdistrict;
use App\Services\Eobce\City as EobceCity;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Http\File;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;

class ProcessCity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     */
    public function __construct(
        public EobceCity $city
    ) {
        //
    }

    /**
     * Execute the job.
     */
    public function handle(): void
    {
        // Load city if needed
        if (!$this->city->isParsed()) $this->city->load();

        // Get data
        $cityData = $this->city->getData();
        $cityData['image'] = self::saveImage($cityData['logoUrl']);

        // Check if subdistrict exists
        $districtId = $this->saveDistrict($this->city);
        $subdistrictId = $this->saveSubdistrict($this->city, $districtId);

        // Include subdistrict if found
        if ($subdistrictId) $cityData['subdistrict_id'] = $subdistrictId;

        self::saveCity($cityData);
    }


    // Save image into storage and return path
    private static function saveImage($url): string
    {
        // Validate URL
        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new \Exception('Invalid image URL');
        }

        // Generate a unique filename
        $fileName = uniqid('image_') . '.jpg';

        // Create temp file
        $tempFilePath = tempnam(sys_get_temp_dir(), $fileName);
        file_put_contents($tempFilePath, file_get_contents($url));

        // Save file to the storage directory
        $filePath = Storage::putFile('public/images', new File($tempFilePath));

        // Clean up the temp file
        Storage::delete($tempFilePath);

        return str_replace('public/', '', $filePath);
    }

    /**
     * Check if city subdistrict exists and create them if not
     * 
     * @param City City to check
     * @return int|null id of District or null if city doesnt have one
     */
    private function saveSubdistrict(EobceCity $city, ?int $districtId): int|null
    {
        // If no subdistrict dont continue
        if (!$this->city->subdistrict) return null;
        $subdistrict = $this->city->subdistrict;

        // Check if subdistrict exists in db
        $subdistrictModel = Subdistrict::where('url', $subdistrict->getUrl())->first();

        if (!$subdistrictModel) {
            // Load data if needed
            if (!$subdistrict->isParsed()) $subdistrict->load();
            $subdistrictData = $subdistrict->getData();
            $subdistrictData['url'] = $subdistrict->getUrl();

            // Create slug
            $slug = Str::slug($subdistrictData['name']);
            $subdistrictData['slug'] = $slug;

            // Include district id if provided
            if ($districtId)
                $subdistrictData['district_id'] = $districtId;

            $subdistrictModel = Subdistrict::create($subdistrictData);
        }

        return $subdistrictModel->id;
    }

    /**
     * Check if district exists and create them if not
     * 
     * @param City City to check
     * @return int|null id of Subdistrict or null if city doesnt have one
     */
    private function saveDistrict(EobceCity $city): int|null
    {
        // If no district dont continue
        if (!$this->city->subdistrict) return null;
        if (!$this->city->subdistrict->district) return null;
        $district = $this->city->subdistrict->district;

        // Check if district exists in db
        $districtModel = District::where('url', $district->getUrl())->first();

        if (!$districtModel) {
            // Load data if needed
            if (!$district->isParsed()) $district->load();
            $districtData = $district->getData();
            $districtData['url'] = $district->getUrl();


            // Create slug
            $slug = Str::slug($districtData['name']);
            $districtData['slug'] = $slug;

            $districtModel = District::create($districtData);
        }

        return $districtModel->id;
    }

    // Create/Update city data
    private static function saveCity($data): bool
    {
        // Check if city exists in db
        $slug = Str::slug($data['name']);
        $data['slug'] = $slug;
        $city = City::where('url', $data['url'])->first();

        if ($city) {
            $oldImagePath = storage_path('app/public/' . $city->image);
            if (Storage::fileExists($oldImagePath)) {
                Storage::delete($oldImagePath);
            }
            $city->update($data);
            return true;
        } else {
            City::create($data);
            return false;
        }
    }
}
