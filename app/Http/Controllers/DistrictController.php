<?php

namespace App\Http\Controllers;

use App\Models\District;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    /**
     * Display the specified resource.
     */
    public function show(District $district)
    {
        $cities = $district->cities()->paginate(16);
        return view('city.index', compact('cities', 'district'));
    }
}
