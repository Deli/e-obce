<?php

namespace App\Http\Controllers;

use App\Models\City;
use Illuminate\Http\Request;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $cities = collect();
        $search = $request->get('search');
        if ($search) {
            $cities = City::where('name', 'like', '%' . $search . '%')->paginate(16);

            // Quick open
            if ($cities->count() == 1) {
                return redirect(route('city.show', $cities[0]->slug));
            }
        } else {
            $cities = City::paginate(16);
        }
        return view('city.index', compact('cities'));
    }


    /**
     * Display the specified resource.
     */
    public function show(City $city)
    {
        return view('city.show', compact('city'));
    }


    public function autocomplete(Request $request)
    {
        $search = $request->get('query');

        $cities = City::where('name', 'like', '%' . $search . '%')->limit(5)->get(['slug', 'name']);

        // Modify the data to include the route URL
        $cities->each(function ($city) {
            $city->route = route('city.show', $city->slug);
        });

        return response()->json($cities);
    }
}
