<?php

namespace App\Http\Controllers;

use App\Models\Subdistrict;
use Illuminate\Http\Request;

class SubdistrictController extends Controller
{
    /**
     * Display the specified resource.
     */
    public function show(Subdistrict $subdistrict)
    {
        $cities = $subdistrict->cities()->paginate(16);

        $district = null;
        if ($subdistrict->district) $district = $subdistrict->district;

        return view('city.index', compact('cities', 'district', 'subdistrict'));
    }
}
