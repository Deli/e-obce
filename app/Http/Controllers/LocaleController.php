<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LocaleController extends Controller
{
    private $supportedLocales = ['sk', 'en'];

    public function set($locale)
    {
        if (in_array($locale, $this->supportedLocales)) {
            session()->put('locale', $locale);
        } else {
            session()->put('locale', config('app.locale'));
        }
        return redirect()->back();
    }
}
