<?php

namespace App\Services\Eobce;

/**
 * Trait for indicating parsing state.
 *
 * This trait provides a flag to indicate whether parsing has been performed on an object.
 */
trait Parseable
{
    protected bool $parsed = false; // Flag to indicate whether parsing has been done

    public function isParsed(): bool
    {
        return $this->parsed;
    }

    public function setParsed(bool $state = true): void
    {
        $this->parsed = $state;
    }
}
