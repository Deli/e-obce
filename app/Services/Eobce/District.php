<?php

namespace App\Services\Eobce;

/**
 * Class representing a district on e-obce.sk
 * 
 * This class allows you to retrieve and parse data for a specific district from its URL on e-obce.sk.
 * 
 * Usage:
 * $district = new District('NR');
 * // OR
 * $district = new District('https://www.e-obce.sk/kraj/NR.html');
 * 
 * $district->load(); // Parse data from url
 * 
 * // To load subdistrict data:
 * $district->loadSubdistricts();
 * 
 * $data = $district->getData(); // Gets array of district data
 */
class District
{
    use HtmlFetcher, Parseable;

    const AVAILABLE_DISTRICTS = [
        'BB',
        'BA',
        'KE',
        'NR',
        'PO',
        'TN',
        'TT',
        'ZA',
    ];


    public string $name;
    public array $subdistricts;


    /**
     * Initializes a new instance of District. 
     *
     * @param string $district URL or Short name of district to crawl i.e. 'NR'
     * @throws \Exception If the provided district is invalid.
     */
    public function __construct(string $district = 'NR')
    {
        $this->setUrl($this->generateDistrictUrl($district));
    }

    /**
     * Loads HTML content from the URL and parses it.
     *
     * @return void
     */
    public function load(): void
    {
        if (!$this->isParsed()) { // Check if parsing has not been done
            $this->getHtmlFromUrl($this->getUrl());
            $this->parseHtml();
            $this->setParsed(true); // Set the flag to true after parsing
        }
    }



    /**
     * Get data of the district.
     *
     * @return array Data of the district.
     */
    public function getData(): array
    {
        if (!$this->isParsed()) {
            // If HTML has not been parsed, return an empty array
            return [
                'url' => $this->getUrl(),
                'loaded' => 'NO',
            ];
        }

        return [
            'loaded' => 'YES',
            'name' => $this->name,
            'subdistricts' => $this->getSubdistrictsData(),
        ];
    }



    /**
     * Load all subdistricts in district.
     *
     * @return void
     */
    public function loadSubdistricts(): void
    {
        if (!$this->isParsed()) return; // Check if parsing has not been done

        foreach ($this->subdistricts as $subDistrict) {
            $subDistrict->load();
        }
    }

    /**
     * Load all subdistricts and cities in district.
     *
     * @return void
     */
    public function loadAll(): void
    {
        if (!$this->isParsed()) return; // Check if parsing has not been done

        foreach ($this->subdistricts as $subDistrict) {
            $subDistrict->load();
            foreach ($subDistrict->cities as $city) {
                $city->load();
            }
        }
    }


    /**
     * Returns count of cities in district
     * 
     * Loads subdistricts then returns count of cities in them
     * 
     * @return int City count
     */
    public function cityCount(): int
    {
        $cityCount = 0;
        foreach ($this->subdistricts as $subdistrict) {
            if (!$subdistrict->isParsed()) $subdistrict->load();
            $cityCount += $subdistrict->cityCount();
        }
        return $cityCount;
    }



    /**
     * Get array of subdistricts data
     * 
     * @return array Array of subdistricts getData() results
     */
    private function getSubdistrictsData(): array
    {
        $subdistrictsData = [];
        foreach ($this->subdistricts as $subdistrict) {
            $subdistrictsData[] = $subdistrict->getData();
        }
        return $subdistrictsData;
    }



    /**
     * Parse loaded html.
     *
     * Sets district parameters from loaded html.
     *
     * @return void
     */
    private function parseHtml(): void
    {
        $this->name = $this->parseName();
        $this->subdistricts = $this->createSubdistricts($this->parseSubdistrictUrls());
    }



    /**
     * Converts array of urls into array of Subdistrict objects.
     * 
     * @param array $subdistrictUrls Array of subdistrict URLs
     * @return array Array of Subdistrict objects
     */
    private function createSubdistricts(array $subdistrictUrls): array
    {
        $subdistricts = [];

        foreach ($subdistrictUrls as $url) {
            $subDistrict = new Subdistrict($url, $this);
            $subdistricts[] = $subDistrict;
        }

        return $subdistricts;
    }



    /**
     * Retrieve subdistricts 
     *  
     * Gets array of subdistrict URLs
     * 
     * @return array Array of subdistrict urls
     */
    private function parseSubdistrictUrls(): array
    {
        // Use regular expression to extract district URLs from the HTML content.
        preg_match_all('/https:\/\/www\.e-obce\.sk\/okres\/([a-zA-Z_]+)\.html/m', $this->getHtml(), $matches);
        // This is possible due to the fact that the site only lists sub-districts for one district. 

        // Return unique subdistrict names and URLs found in the HTML content.
        return array_unique($matches[0]);
    }



    /**
     * Checks if the provided district is valid.
     *
     * @param string $district The short name of the district to check.
     * @return bool True if valid.
     */
    private static function checkDistrict(string $district): bool
    {
        if (in_array($district, self::AVAILABLE_DISTRICTS)) {
            return true;
        }
        return false;
    }



    private function generateDistrictUrl(string $district): string
    {
        if ($this->checkDistrict($district)) {
            return "https://www.e-obce.sk/kraj/$district.html";
        } else {
            if (filter_var($district, FILTER_VALIDATE_URL) === FALSE) {
                throw new \Exception("Invalid url, use short name of district (i.e. 'KE', 'NR', ...) or full district url (i.e. 'https://www.e-obce.sk/kraj/NR.html')");
            }
            return $district;
        }
    }



    private function parseName(): string
    {
        preg_match("/<title>(.*?)(?: - E-OBCE\.sk)<\/title>/su", $this->getHtml(), $matches);
        if (sizeof($matches))
            return trim($matches[1]);
        return '';
    }
}
