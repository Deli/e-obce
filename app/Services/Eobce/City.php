<?php

namespace App\Services\Eobce;

/**
 * Class representing a city on e-obce.sk
 * 
 * This class allows you to retrieve and parse data for a specific city from its URL on e-obce.sk.
 * 
 * Usage:
 * $city = new City($url);
 * $city->load(); // Parse data from url
 * $data = $city->getData(); // Gets array of city data
 */
class City
{
    use HtmlFetcher, Parseable;

    public string $name;
    public string $logoUrl;
    public string $hall_address;
    public string $web;
    public string $email;
    public string $mayor;
    public string $phone;
    public string $fax;
    // public string $ico;
    // public string $chief;
    // public string $region;
    // public string $pocet_obyvatelov;
    // public string $rozloha;
    public Subdistrict $subdistrict;


    /**
     * Initializes a new instance of City and sets the url property.
     *
     * @param string|null $url The URL of city.
     * @return void
     */
    public function __construct(string $url = null, ?Subdistrict $subdistrict = null)
    {
        $this->setUrl($url);
        if ($subdistrict) $this->subdistrict = $subdistrict;
    }



    /**
     * Loads HTML content from the URL and parses it.
     *
     * @return void
     */
    public function load(): void
    {
        if (!$this->isParsed()) { // Check if parsing has not been done
            $this->getHtmlFromUrl($this->getUrl());
            $this->parseHtml();
            $this->setParsed(true); // Set the flag to true after parsing
        }
    }



    /**
     * Get data of the city.
     *
     * @return array Data of the city.
     */
    public function getData(): array
    {
        if (!$this->isParsed()) {
            // If HTML has not been parsed, return an empty array
            return [
                'url' => $this->getUrl(),
                'loaded' => 'NO',
            ];
        }

        $data = [
            'url' => $this->url,
            'loaded' => 'YES',
            'name' => $this->name,
            'hall_address' => $this->hall_address,
            'web' => $this->web,
            'email' => $this->email,
            'mayor' => $this->mayor,
            'phone' => $this->phone,
            'fax' => $this->fax,
            'logoUrl' => $this->logoUrl,
            // 'ico' => $this->ico,
            // 'region' => $this->region,
            // 'prednosta' => $this->prednosta,
            // 'rozloha' => $this->rozloha,
            // 'pocet_obyvatelov' => $this->pocet_obyvatelov,
        ];


        return $data;
    }



    /**
     * Parse loaded html.
     *
     * Sets city parameters from loaded html.
     *
     * @return void
     */
    private function parseHtml(): void
    {
        $this->name = $this->parseName();
        $this->logoUrl = $this->parseInsigniaUrl();
        $this->hall_address = $this->parseHallAddress();
        $this->web = $this->parseWebAddress();
        $this->email = $this->parseEmail();
        $this->parseBasicInfo();
    }



    /**
     * Parse table data based on 'key'.
     * 
     * Returns HTML of td element next to td element containing $key
     * 
     * @param string $key Text from td element to search
     * @return string Contents of td element next to td element containing $key or '' if not found
     */
    private function parseTableData(string $key): string
    {
        preg_match("/<td[^>]*?>.*?$key.*?<\/td>\s*<td[^>]*?>(.*?)<\/td>/su", $this->getHtml(), $matches);
        if (sizeof($matches))
            return trim($matches[1]);
        return '';
    }



    private function parseName(): string
    {
        preg_match('/<h1>(?:Mesto|Obec)(.*?)</s', $this->getHtml(), $matches);
        if (!empty($matches))
            return trim($matches[1]);
        return '';
    }



    private function parseHallAddress(): string
    {
        preg_match('/"telo".*?<td[^>].*?<table[^>]*?>(?:.*?<tr[^>]*>){2}.*?(?:Mestsk.|Obecn.) .rad(?:.*?<tr[^>]*>){3}.*?<td[^>]*?>(.*?)<\/td/su', $this->getHtml(), $matches);
        if (!empty($matches))
            return trim($matches[1]);
        return '';
    }



    private function parseInsigniaUrl(): string
    {
        preg_match('/http(?:s):\/\/www\.e-obce\.sk\/erb\/(.+\.(?:gif|jpg|jpeg|png))/', $this->getHtml(), $matches);
        if (!empty($matches))
            return trim($matches[0]);
        return '';
    }



    private function parseWebAddress(): string
    {
        $webHtml = $this->parseTableData("Web");
        preg_match('/href=[\'\"](.+?)[\"\']/', $webHtml, $matches);
        if (!empty($matches))
            return trim($matches[1]);
        return '';
    }



    private function parseEmail(): string
    {
        $webHtml = $this->parseTableData("Email");
        preg_match('/>(.+?@.+?\..{2,}?)?<\//', $webHtml, $matches);
        if (!empty($matches))
            return trim($matches[1]);
        return '';
    }



    /**
     * Parse basic data of city and sets city properties.
     * 
     * @return void
     */
    private function parseBasicInfo(): void
    {
        $basicInfo = [
            'mayor' => 'Starosta',
            'mayor2' => 'Prim.tor',
            'phone' => 'Mobil',
            'fax' => 'Fax',

            // Unused:
            // 'region' => 'Región',
            // 'ico' => 'I.O',
            // 'chief' => 'Prednosta',
            // 'citizen_count' => 'Po.et obyvate.ov',
            // 'area' => 'Rozloha',
        ];

        // Initialize an array to store the parsed city data
        $cityData = [];

        // Use parseTableData() to get most of the data
        foreach ($basicInfo as $key => $search) {
            $match = $this->parseTableData($search);
            $cityData[$key] = $match;
        }

        // Check/fix problematic data

        // Starosta/Primator
        if ($cityData['mayor']) {
            $this->mayor = $cityData['mayor'];
        } elseif ($cityData['mayor2']) {
            $this->mayor = $cityData['mayor2'];
        }

        // Mobile/Phone check
        if (!$cityData['phone']) {
            preg_match('/<td[^>]*?>.*?Tel.*?(\d{3,}\s*?\/\s*?\d{3,}.*?\d{2}.*?\d{2}.*?)/s', $this->getHtml(), $matches);
            if (!empty($matches))
                $cityData['phone'] = trim($matches[1]);
        }


        // Assign the parsed city data to the respective properties of the City.
        $this->phone = $cityData['phone'] ?? '';
        $this->fax = $cityData['fax'] ?? '';

        // Unused:
        // $this->ico = $cityData['ico'] ?? '';
        // $this->region = $cityData['region'] ?? '';
        // $this->chief = $cityData['chief'] ?? '';
        // $this->citizen_count = $cityData['citizen_count'] ?? '';
        // $this->area = $cityData['area'] ?? '';
    }
}
