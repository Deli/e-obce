<?php

namespace App\Services\Eobce;

/**
 * Trait for fetching HTML content from URLs.
 *
 * This trait provides methods to fetch HTML content from URLs and handle encoding conversion if necessary.
 */
trait HtmlFetcher
{
    protected string $url;
    protected string $html;


    public function setUrl(string $url): void
    {
        $this->url = $url;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setHtml(string $html): void
    {
        $this->html = $html;
    }

    public function getHtml(): string
    {
        return $this->html;
    }



    /**
     * Fetch HTML content from a URL.
     *
     * Retrieves HTML content from the specified URL and converts it to UTF-8 encoding if necessary.
     *
     * @param string $url The URL from which to fetch HTML content.
     * @param string $sourceEncoding (Optional) The encoding of the HTML content (default: Windows-1250).
     * @return string HTML content fetched from the provided URL.
     */
    public function getHtmlFromUrl(string $url, string $sourceEncoding = 'Windows-1250'): string
    {
        // Fetch HTML content from the URL
        $html = file_get_contents($url);

        // Convert HTML content to UTF-8 encoding
        $utf8html = iconv($sourceEncoding, 'UTF-8', $html);

        // Save and return
        $this->setHtml($utf8html);
        return $utf8html;
    }
}
