<?php

namespace App\Services\Eobce;

/**
 * Class representing a subdistrict on e-obce.sk
 * 
 * This class allows you to retrieve and parse data for a specific subdistrict from its URL on e-obce.sk.
 * 
 * Usage:
 * $subdistrict = new Subdistrict('NR');
 * // OR
 * $subdistrict = new Subdistrict('https://www.e-obce.sk/kraj/NR.html');
 * 
 * $subdistrict->load(); // Parse data from url
 * 
 * // To load city data:
 * $subdistrict->loadCities();
 * 
 * $data = $subdistrict->getData(); // Gets array of subdistrict data
 */
class Subdistrict
{
    use HtmlFetcher, Parseable;

    public string $name;
    public array $cities;
    public District $district;


    /**
     * Initializes a new instance of Subdistrict. 
     *
     * @param string URL of district to parse
     * @param string URL of district to parse
     */
    public function __construct(string $url = null, ?District $district = null)
    {
        $this->setUrl($url);
        if ($district) $this->district = $district;
    }



    /**
     * Loads HTML content from the URL and parses it.
     *
     * @return void
     */
    public function load(): void
    {
        if (!$this->isParsed()) { // Check if parsing has not been done
            $this->getHtmlFromUrl($this->getUrl());
            $this->parseHtml();
            $this->setParsed(true); // Set the flag to true after parsing
        }
    }



    /**
     * Get data of the subdistrict.
     *
     * @return array Data of the subdistrict.
     */
    public function getData(): array
    {
        if (!$this->isParsed()) {
            // If HTML has not been parsed, return an empty array
            return [
                'url' => $this->getUrl(),
                'loaded' => 'NO',
            ];
        }

        $data = [
            'url' => $this->url,
            'loaded' => 'YES',
            'name' => $this->name,
            'cities' => $this->getCitiesData(),
        ];

        // Include district data if the district object is set
        if (isset($this->district) && $this->district->isParsed()) {
            $data['district'] = $this->district->name;
        }

        return $data;
    }



    /**
     * Load all cities in subdistrict.
     *
     * @return void
     */
    public function loadCities(): void
    {
        if (!$this->isParsed()) return; // Check if parsing has not been done

        foreach ($this->cities as $city) {
            $city->load();
        }
    }



    /**
     * Returns count of cities in subdistrict
     * 
     * @return int City count
     */
    public function cityCount(): int
    {
        return count($this->cities);
    }



    /**
     * Get array of cities data
     * 
     * @return array Array of cities getData() results
     */
    private function getCitiesData(): array
    {
        $citiesData = [];
        foreach ($this->cities as $city) {
            $citiesData[] = $city->getData();
        }
        return $citiesData;
    }



    /**
     * Parse loaded html.
     *
     * Sets subdistrict parameters from loaded html.
     *
     * @return void
     */
    private function parseHtml(): void
    {
        $this->name = $this->parseName();
        $this->cities = $this->createCitiesFromUrls($this->parseCityUrls());
    }



    /**
     * Converts array of urls into array of City objects.
     * 
     * @param array $cityUrls Array of city URLs
     * @return array Array of city objects
     */
    private function createCitiesFromUrls(array $cityUrls): array
    {
        $cities = [];

        foreach ($cityUrls as $url) {
            $city = new City($url, $this);
            $cities[] = $city;
        }

        return $cities;
    }



    /**
     * Retrieve city URLs 
     *  
     * Gets array of city URLs
     * 
     * @return array Array of city urls
     */
    private function parseCityUrls(): array
    {
        // Use regular expression to extract district URLs from the HTML content.
        // This is possible due to the fact that the site only lists sub-districts for one district. 
        preg_match_all('/https:\/\/www\.e-obce\.sk\/obec\/([a-zA-Z_]+)\/(?:[a-zA-Z_]+)\.html/m', $this->getHtml(), $matches);

        // Return unique city URLs found in the HTML content.
        return array_unique($matches[0]);
    }



    private function parseName(): string
    {
        preg_match("/Vyberte si obec alebo mesto z okresu (.+?):/su", $this->getHtml(), $matches);
        if (sizeof($matches))
            return trim($matches[1]);
        return '';
    }
}
