<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'url',
    ];

    protected $hidden = ['url'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function subdistricts()
    {
        return $this->hasMany(Subdistrict::class);
    }

    public function cities()
    {
        return $this->hasManyThrough(City::class, Subdistrict::class);
    }
}
