<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subdistrict extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'url',
        'district_id',
    ];

    protected $hidden = ['url'];

    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function district()
    {
        return $this->belongsTo(District::class);
    }

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
