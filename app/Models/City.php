<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'slug',
        'url',
        'hall_address',
        'mayor',
        'phone',
        'fax',
        'web',
        'email',
        'image',
        'lat',
        'lng',
        'subdistrict_id',
    ];

    protected $hidden = ['url'];



    /**
     * Get the route key for the model.
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    public function subdistrict()
    {
        return $this->belongsTo(Subdistrict::class);
    }

    public function district()
    {
        if ($this->subdistrict)
            return $this->subdistrict->district();
        return null;
    }

    public function geocode(): array|null
    {
        $apiKey = env('GOOGLE_API_KEY', null);
        if (!$apiKey) return null;

        $urlAddress = urlencode($this->name);
        $data = file_get_contents("https://maps.googleapis.com/maps/api/geocode/json?address=$urlAddress,Slovakia&key=$apiKey");
        $geoData = json_decode($data);
        if ($geoData->status == "OK") {
            $location = $geoData->results[0]->geometry->location;
            $update = ['lat' => $location->lat, 'lng' => $location->lng];
            $this->update($update);
        }
        return null;
    }
}
