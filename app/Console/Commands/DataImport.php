<?php

namespace App\Console\Commands;

use App\Jobs\ProcessCity;
use App\Services\Eobce\District;
use Illuminate\Console\Command;


class DataImport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:import {district?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import data from e-obce.sk';

    /**
     * Execute the console command.
     */
    public function handle()
    {

        // Check arguments
        $district = $this->argument('district');
        if ($district) {
            $district = strtoupper($district);
        } else {
            $district = 'NR'; // default
        }


        // Init crawler and load District
        $district = new District($district);
        $district->load();

        // Next load subdistricts
        $district->loadSubdistricts();

        // Count all cities and init counter
        $totalCount = $district->cityCount();
        $currentCount = 0;



        $this->info("$totalCount urls ready to be checked");

        foreach ($district->subdistricts as $subdistrict) {
            foreach ($subdistrict->cities as $city) {
                $currentCount++;
                $this->info("($currentCount/$totalCount) Loading " . $city->getUrl());
                ProcessCity::dispatch($city);
            }
        }
    }
}
