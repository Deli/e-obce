<?php

namespace App\Console\Commands;

use App\Jobs\GeocodeCity;
use App\Models\City;
use Illuminate\Console\Command;

class DataGeocode extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'data:geocode';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Lat/Lng data for cities using google geocode api';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // API Key check
        if (!env('GOOGLE_API_KEY', null)) {
            $this->error("Missing API Key in .env");
            return;
        }

        // Load cities that werent geocoded yet
        $cities = City::where("lat", null)->orWhere("lng", null)->get();

        // Count all cities
        $totalCount = $cities->count();
        $currentCount = 0;
        $this->info("$totalCount urls ready to be geocoded");

        foreach ($cities as $city) {
            $currentCount++;
            $this->info("($currentCount/$totalCount) Geocoding '$city->name'");
            GeocodeCity::dispatch($city);
        }
    }
}
