<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\City>
 */
class CityFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $name = fake()->city();

        return [
            'name' => $name,
            'slug' => Str::slug($name),
            'url' => fake()->url(),
            'hall_address' => fake()->address(),
            'mayor' => fake()->name(),
            'phone' => fake()->phoneNumber(),
            'email' => fake()->email(),
            'fax' => fake()->phoneNumber(),
            'web' => fake()->url(),
            'image' => str_replace('public/storage/', '', fake()->image('public/storage/images'))

        ];
    }
}
