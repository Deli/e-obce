<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\District;
use App\Models\Subdistrict;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $district = District::factory();
        $subdistricts = Subdistrict::factory()->for($district)->count(4)->create();
        foreach ($subdistricts as $subdistrict) {
            City::factory()
                ->count(2)
                ->for($subdistrict)
                ->create();
        }
    }
}
