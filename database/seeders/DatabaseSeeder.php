<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\File;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {

        // Get all files and folders in the images directory
        $files = File::allFiles(public_path('storage/images'));

        // Loop through each file
        foreach ($files as $file) {
            // Get the file path
            $filePath = $file->getPathname();

            // Check if the file is not the .gitkeep file
            if (basename($filePath) !== '.gitkeep') {
                // Delete the file
                File::delete($filePath);
            }
        }


        $this->call(CitySeeder::class);
    }
}
